import java.util.Calendar;
import java.util.GregorianCalendar;

public class Car extends Vehicle {
    private int power = 0;
    private Calendar productionDate;

    public Car(){
        productionDate = new GregorianCalendar();

    }

    public Car(String name, String colour, int model, int price, String serialNumber, int direction, int power) {
        super(name, colour, model, price, serialNumber, direction);
        this.power = power;
        productionDate = new GregorianCalendar();
    }
    @Override
    public void setAllFields() {
        System.out.println("Input car data");
        System.out.println("Name: ");
        setName(input.nextLine());
        System.out.println("Colour: ");
        setColour(input.nextLine());
        System.out.println("Price");
        setPrice(input.nextInt());
        System.out.println("Model: ");
        setModel(input.nextInt());
        System.out.println("Serial #: ");
        input.nextLine();
        setSerialNumber(input.nextLine());
        System.out.println("Power: ");
        power = input.nextInt();
    }
    public void turnLeft(int degrees) {
        if(degrees < 361 && degrees > -1) {
            if ((getDirection() + degrees) > 361) {
                setDirection(getDirection() + degrees - 360);
            } else {
                setDirection(getDirection() + degrees);
            }
        }
    }

    public void turnRight(int degrees) {
        if(degrees < 361 && degrees > -1) {
            if ((getDirection() - degrees) < -1 ) {
                setDirection(getDirection() - degrees + 360);
            } else {
                setDirection(getDirection() - degrees);
            }
        }
    }

    public void setProductionDate(Calendar calendar) {
        productionDate = calendar;
    }

    public Calendar getProductionDate() {
        return productionDate;
    }
    public void setPower(int power) {
        this.power = power;
    }
    public int getPower() {
        return power;
    }

    //Deep copy
    @Override
    protected Object clone() throws CloneNotSupportedException {
        Car car = (Car) super.clone();
        return car;
    }
    @Override
    public String toString() {
        return "Name: " + getName() + " Colour: " + getColour() + " Price: " + getPrice() + " Model: " + getModel() + " Serial: " + getSerialNumber() + " Direction: " + getDirection() + " Power: " + power + " Production date: " + productionDate.getTime();
    }
}
