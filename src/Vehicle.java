import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;
public abstract class Vehicle implements Comparable<Vehicle>, Cloneable {
    private String colour, name, serialNumber = "";
    private int model, price, direction;
    private double speed;
    protected Scanner input = new Scanner(System.in);
    Calendar buyingDate;

    public Vehicle() {
        buyingDate = new GregorianCalendar();
        speed = 0;
    }

    Vehicle(String name, String colour, int model, int price, String serialNumber, int direction) {
        this.colour = colour;
        this.name = name;
        this.model = model;
        this.price = price;
        this.serialNumber = serialNumber;
        this.direction = direction;
        speed = 0;
        buyingDate = new GregorianCalendar();
    }
    public void setAllFields() {
        System.out.println("Input vehicle data");
        System.out.println("Name: ");
        name = input.nextLine();
        System.out.println("Colour: ");
        colour = input.nextLine();
        System.out.println("Price");
        price = input.nextInt();
        System.out.println("Model: ");
        model = input.nextInt();
        System.out.println("Serial #: ");
        serialNumber = input.nextLine();
    }
    public abstract void turnLeft(int degrees);
    public abstract void turnRight(int degrees);

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public int getModel() {
        return model;
    }

    public void setModel(int model) {
        this.model = model;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getDirection() {
        return direction;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }
    public void setBuyingDate(Calendar date) {
        buyingDate = date;
    }
    public String getBuyingDate() {
        return Integer.toString(buyingDate.get(Calendar.DATE));
    }
    @Override
    public int compareTo(Vehicle vehicle) {
        if(vehicle.getPrice() < price) return 1;
        else if(vehicle.getPrice() > price) return -1;
        else return 0;
    }

    //Shallow Copy
    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();

    }
    @Override
    public String toString() {
        return "Name of vehicle: " + name +"\nName: " + name + " Colour: " + colour + " Price: " + price + " Model: " + model + " Serial: " + serialNumber + " Direction: " + direction;
    }
}
