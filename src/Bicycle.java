import java.util.Calendar;
import java.util.GregorianCalendar;

public class Bicycle extends Vehicle {
    private int gears;
    private Calendar productionDate;
    public Bicycle(){
        productionDate = new GregorianCalendar();

    }
    public Bicycle(String name, String colour, int model, int price, String serialNumber, int direction, int gears) {
        super(name, colour, model, price, serialNumber, direction);
        this.gears = gears;
        productionDate = new GregorianCalendar();

    }
    //kommentar
    @Override
    public void setAllFields() {
        System.out.println("Input Bicycle data");
        System.out.println("Name: ");
        setName(input.nextLine());
        System.out.println("Colour: ");
        setColour(input.nextLine());
        System.out.println("Price");
        setPrice(input.nextInt());
        System.out.println("Model: ");
        setModel(input.nextInt());
        System.out.println("Serial #: ");
        input.nextLine();
        setSerialNumber(input.nextLine());
        System.out.println("Gears: ");
        gears = input.nextInt();
    }

    public void turnRight(int degrees) {
        System.out.println("Bicycle has turned " + degrees + " degrees right.");
    }
    public void turnLeft(int degrees) {
        System.out.println("Bicycle has turned " + degrees + " degrees left.");
    }
    public void setProductionDate(Calendar calendar) {
        productionDate = calendar;
    }

    public Calendar getProductionDate() {
        return productionDate;
    }
    public int getGears() {
        return gears;
    }

    public void setGears(int gears) {
        this.gears = gears;
    }

    //Deep copy
    @Override
    protected Object clone() throws CloneNotSupportedException {
        Bicycle bicycle = (Bicycle) super.clone();
        return bicycle;
    }

    @Override
    public String toString() {
        return "Name: " + getName() + " Colour: " + getColour() + " Price: " + getPrice() + " Model: " + getModel() + " Serial: " + getSerialNumber() + " Direction: " + getDirection() + " Gears:  " + gears + " Production date: " + productionDate.getTime();
    }
}
