/**
 *  TestVehicles oppretter Bicycle og Car objekter, legger disse i et
 ArrayList
 *  Lar bruker manipulere data i arrayet på forskjellige måter
 */
import java.util.*;
import java.io.*;
public class VehicleTest {
    public static void main(String[] args) {
        VehicleTest vtest = new VehicleTest();
        try {
            vtest.menuLoop();
        } catch(IOException e) {
            System.out.println("IO Exception!");
            System.exit(1);
        } catch(CloneNotSupportedException e) {
            System.out.println("CloneNotSupportedException");
            System.exit(1);
        }
    }
    private void menuLoop() throws IOException, CloneNotSupportedException {
        Scanner scan = new Scanner(System.in);
        ArrayList<Vehicle> arr=new ArrayList<Vehicle>();
        Vehicle vehicle;
        arr.add(new Car("Volvo","Black",85000,2010,"1010-11",163,0));
        arr.add(new Bicycle("Diamant","yellow",4000,1993,"BC100",10,0));
        arr.add(new Car("Ferrari Testarossa","red",1200000,1996,"A112",350,0));
        arr.add(new Bicycle("DBS","pink",5000,1994,"42",10,0));
        Car car1 = new Car("Lamborghini", "Yellow", 2001, 2900000, "1337", 61, 650);
        Car car2 = null;
        car2 = (Car) car1.clone();
        car1.setBuyingDate(new GregorianCalendar(1999, 11, 29));


        while(true) {
            System.out.println("1...................................New car");
            System.out.println("2...............................New bicycle");
            System.out.println("3......................Find vehicle by name");
            System.out.println("4..............Show data about all vehicles");
            System.out.println("5.......Change direction of a given vehicle");
            System.out.println("6.........................Test clone method");
            System.out.println("7..............................Exit program");
            System.out.println(".............................Your choice?");
            int choice = scan.nextInt();
            switch (choice) {
                case 1:
                    arr.add(new Car());
                    arr.get(arr.size() - 1).setAllFields();
                    break;
                case 2:
                    arr.add(new Bicycle());
                    arr.get(arr.size() - 1).setAllFields();
                    break;
                case 3:
                    System.out.println("Name of vehicle: ");
                    String name = scan.nextLine();
                    for (Vehicle v : arr) {
                        if (v.getName() == name) {
                            System.out.println(v.toString());
                        }
                    }
                    break;
                case 4:
                    for (Vehicle v : arr) {
                        System.out.println(v.toString());
                    }
                    break;
                case 5:
                    System.out.println("Name of vehicle:");
                    scan.nextLine();
                    String name2 = scan.nextLine();
                    int temp = -1;
                    for (int i = 0; i < arr.size(); i++) {
                        if (name2.equals(arr.get(i).getName())) {
                            temp = i;
                        }
                    }
                    if (temp == -1) {
                        System.out.println("Not found");
                        break;
                    }
                    System.out.println("Direction [R/L]: ");
                    String direction = scan.nextLine();
                    System.out.println("Degrees [0-360]: ");
                    scan.nextInt();

                    if(direction.equals("L")) arr.get(temp).turnLeft(scan.nextInt());
                    if(direction.equals("R")) arr.get(temp).turnRight(scan.nextInt());
                    break;
                case 6:
                    scan.close();
                    System.exit(0);
                case 7:
                    System.out.format("Date objects are seperate, deep copy\n%s\n%s", car1.getBuyingDate(), car2.getBuyingDate());
                default:
                    System.out.println("Wrong input!");
            }
        }
    }
}